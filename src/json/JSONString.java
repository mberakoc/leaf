package json;

public class JSONString {

    private String string;

    public JSONString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "\"" + string + "\"";
    }
}

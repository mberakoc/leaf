package json;

import java.io.FileWriter;
import java.io.IOException;

public class Factory {

    public static boolean isInArray;
    private final static int DEFAULT_LEVEL = 1;
    public static int level = DEFAULT_LEVEL;
    public static JSONObject root = JSONObject.createObject();
    static FileWriter jsonFileWriter;

    private Factory() {}

    public static void writeInJSONFile(String filename) {
        root.write(filename, root.toString());
        try {
            jsonFileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.printf("JSON file created: [%s.json]\n", filename);
    }
}

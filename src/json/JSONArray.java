package json;

public class JSONArray {

    private Object[] array;

    private JSONArray(Object[] array) {
        this.array = array;
    }

    public static JSONArray createJSONArray(Object[] array) {
        return new JSONArray(array);
    }

    @Override
    public String toString() {
        Factory.isInArray = true;
        int level = Factory.level;
        ++Factory.level;
        StringBuilder JSONArrayString = new StringBuilder("[\n");
        for (int i = 0; i < array.length; ++i) {
            String arrayString = String.format("%s%s%s\n", "\t".repeat(Math.max(0, level)), array[i],
                    i == array.length - 1 ? "": ",");
            JSONArrayString.append(arrayString);
        }
        JSONArrayString.append("\t".repeat(Math.max(0, level - 2))).append("]");
        Factory.isInArray = false;
        return JSONArrayString.toString();
    }
}

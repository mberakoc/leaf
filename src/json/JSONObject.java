package json;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class JSONObject {

    private ArrayList<JSONNode> children;

    private JSONObject() {
        children = new ArrayList<>();
    }

    public static JSONObject createObject() {
        return new JSONObject();
    }

    public JSONObject addNode(JSONNode JSONNode) {
        children.add(JSONNode);
        return this;
    }

    public void write(String filename, String data) {
        if (!filename.contains(".json")) filename += ".json";
        try
        {
            if (Factory.jsonFileWriter == null) Factory.jsonFileWriter = new FileWriter(new File(filename), false);
            Factory.jsonFileWriter.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        int level = Factory.level;
        if (!Factory.isInArray) ++Factory.level;
        StringBuilder JSONObjectString = new StringBuilder("{\n");
        for (int i = 0; i < children.size(); ++i) {
            JSONNode currentJSONNode = children.get(i);
            String nodeString = String.format("%s\"%s\": %s%s\n", "\t".repeat(Math.max(0, level)), currentJSONNode.key, currentJSONNode.value,
                    i == children.size() - 1 ? "": ",");
            JSONObjectString.append(nodeString);
        }
        JSONObjectString.append("\t".repeat(Math.max(0, level - 1))).append("}");
        return JSONObjectString.toString();
    }
}

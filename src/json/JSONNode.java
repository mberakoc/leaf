package json;

public class JSONNode {

    String key;
    Object value;

    private JSONNode(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public static JSONNode createNode(String key, Object value) {
        return new JSONNode(key, value);
    }

    public void injectValue(Object value) {
        this.value = value;
    }
}

package main;

import json.*;

public class Main {

    public static void main(String[] args) {
        Factory.root.addNode(JSONNode.createNode("name", new JSONString("Adam Smith")));
        Factory.root.addNode(JSONNode.createNode("age", 35));
        Factory.root.addNode(JSONNode.createNode("isWealthy", false));
        JSONObject address = JSONObject.createObject();
        address.addNode(JSONNode.createNode("street", new JSONString("Courtyard Street")));
        address.addNode(JSONNode.createNode("city", new JSONString("London")));
        address.addNode(JSONNode.createNode("country", new JSONString("England")));
        Factory.root.addNode(JSONNode.createNode("address", address));
        JSONObject[] children = new JSONObject[]{
                JSONObject.createObject()
                .addNode(JSONNode.createNode("name", new JSONString("Julia Smith")))
                .addNode(JSONNode.createNode("age", 12)),
                JSONObject.createObject()
                .addNode(JSONNode.createNode("name", new JSONString("Noah Smith")))
                .addNode(JSONNode.createNode("age", 19))};
        Factory.root.addNode(JSONNode.createNode("children", JSONArray.createJSONArray(children)));
        Factory.writeInJSONFile("user");
    }
}

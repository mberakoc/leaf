# [Leaf](https://www.json.org/json-en.html) <br>[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-ffd800.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Generic badge](https://img.shields.io/badge/version-v1.0.1-blueviolet.svg)](https://shields.io/) [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-brightgreen.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
A core library for Java to handle JSON.
* __Easy To Learn:__ Library is designed to provide a smooth learning curve. No need to spend days on API to create a tiny application. It has a very clear and concise way to build JSON Files.
* __Polymorphic:__ Except the String type all other values are straightforward. You don't have to box values in pseudo-type classes. 
* __Two Way Conversion:__ Your app changes JSON dynamically? No worries. Leaf can capability to convert Java objects to JSON format or converting A JSON file into Java Objects. Also you can use the predefined methods to manipulate JSON data.
# Documentation
Leaf is fully-documented with JavaDoc.

Check out the [Getting Started](http://localhost/ctring/template/introduction.html) for quick review.

## Licence 
Symbol is [GNU Licensed.](https://gitlab.com/mberakoc/leaf/blob/master/LICENSE)